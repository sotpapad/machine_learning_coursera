Implementation of the programming exercises of the Machine Learning course offered by Coursera.

The codes in this project correspond to valid solutions for users of Gnu-Octave (>= 3.8.0) and/or MATLAB < 2019b.
